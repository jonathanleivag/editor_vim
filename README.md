# Configuración de la terminal

```sh
cd .config
git clone https://gitlab.com/jonathanleivag1/cedecap-api.git
```

## instalar ohmyz
[Oh my zsh](https://ohmyz.sh/)
* crear un enlace simbolico
```sh
ln -s .zshrc -> .config/nvim/.zshrc
```

## instalar nvim
 
 ```sh
brew install neovim
ln -s .vimrc -> .config/nvim/.vimrc
 ```

 ## instalar fig
 [fig](https://fig.io/)

 ## vim-plug
 [vim-Plug](https://github.com/junegunn/vim-plug)

 ```sh
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
 ```

 ## Instalar la fuente que se llama *PowerlineSymbols.otf
 usarla en la terminal esa fuente

 ## install coc 
 Install extensions like this:
 ```sh
 :CocInstall coc-json coc-tsserver
```
